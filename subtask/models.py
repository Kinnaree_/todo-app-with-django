from django.db import models

from todo.models import Task


# Create your models here.
class SubTask(models.Model):
    title = models.CharField(max_length=100)
    detail = models.TextField(max_length=200, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    completion_date = models.DateField(null=True, blank=True)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    complete = models.BooleanField(default=False)

    def __str__(self):
        return self.title