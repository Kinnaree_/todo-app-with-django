from django import forms

from subtask.models import SubTask
from todo.models import Task
from todo.validation import validate_date


class SubTaskForm(forms.ModelForm):
    completion_date = forms.DateField(
        required=False,
        validators=[validate_date]
    )

    class Meta:
        model = SubTask
        fields = ('title', 'detail', 'completion_date', 'task')
        widgets = {'task': forms.HiddenInput()}


class UpdateSubTaskForm(forms.ModelForm):
    completion_date = forms.DateField(
        required=False,
        validators=[validate_date]
    )

    class Meta:
        model = SubTask
        fields = '__all__'
        widgets = {'task': forms.HiddenInput()}


class MoveTaskForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        request = kwargs.pop('request')
        super(MoveTaskForm, self).__init__(*args, **kwargs)
        sub_task = kwargs.get('instance')
        task_id = sub_task.task.id
        self.fields['task'] = forms.ModelChoiceField(queryset=Task.objects.filter(tag__owner=request.user).exclude(id=task_id))

    class Meta:
        model = SubTask
        fields = ('task',)


