from django.urls import reverse, resolve

from subtask.views import ListSubTaskView, CreateSubTaskView, UpdateSubTaskView, DeleteSubTaskView, MoveSubTaskView


class TestSubTaskUrls:
    """Test for subtask urls.
    Below test functions tests for all urls defined in subtask/urls.py
    """

    def test_subtask_get_url(self):
        url = reverse('list-subtask', kwargs={'id': 1})
        assert resolve(url).func.view_class == ListSubTaskView

    def test_subtask_add_url(self):
        url = reverse('add-subtask', kwargs={'id': 1})
        assert resolve(url).func.view_class == CreateSubTaskView

    def test_subtask_update_url(self):
        url = reverse('update-subtask', kwargs={'pk': 1})
        assert resolve(url).func.view_class == UpdateSubTaskView

    def test_subtask_delete_url(self):
        url = reverse('delete-subtask', kwargs={'pk': 1})
        assert resolve(url).func.view_class == DeleteSubTaskView

    def test_subtask_move_url(self):
        url = reverse('move-subtask', kwargs={'pk': 1})
        assert resolve(url).func.view_class == MoveSubTaskView

