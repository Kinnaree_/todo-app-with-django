import pytest
from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed, assertRedirects

from todo.models import Task


class TestSubTaskView:

    @pytest.mark.django_db
    def test_get_request(self, client, user_login, create_subtask):
        url = reverse('list-subtask', kwargs={'id': create_subtask.id})
        response = client.get(url)
        assert response.context['task_id'] == 1
        assert response.context['tag_id'] == 1
        assert response.context['sub_tasks'] == response.context['sub_tasks']
        assert response.status_code == 200
        assertTemplateUsed(response, 'subtask/list_subtask.html')

    @pytest.mark.django_db
    def test_get_subtask_request_without_login_user(self, client):
        response = client.post(reverse('list-subtask', kwargs={'id': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_create_subtask(self, client, user_login, create_task):
        t = Task.objects.get()
        data = {
            "title": "practice",
            "detail": "learn",
            "task": t.id,
            "completion_date": "2023-02-24"
        }
        url = reverse('add-subtask', kwargs={'id': t.id})
        response = client.post(url, data=data)
        assert response.status_code == 302
        assertRedirects(response, reverse('list-subtask', kwargs={'id': t.id}))

    @pytest.mark.django_db
    def test_create_subtask_request_without_login_user(self, client):
        response = client.post(reverse('add-subtask', kwargs={'id': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_create_subtask_with_404(self, client, user_login, create_task):
        response = client.post('add/subtask/dfdf/')
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_update_subtask(self, client, user_login, create_subtask):
        t = Task.objects.get()
        data = {
            "title": "salary",
            "detail": "learn",
            "task": t.id,
            "complete": "True",
            "completion_date": "2023-02-24"
        }
        url = reverse('update-subtask', kwargs={'pk': create_subtask.id})
        response = client.post(url, data=data)
        assert response.status_code == 302
        assertRedirects(response, reverse('list-subtask', kwargs={'id': t.id}))

    @pytest.mark.django_db
    def test_update_subtask_request_without_login_user(self, client):
        response = client.post(reverse('update-subtask', kwargs={'pk': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_update_subtask_with_404(self, client, user_login, create_subtask):
        response = client.post(reverse('update-subtask', kwargs={'pk': 1000}))
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_update_subtask_with_500(self, client, user_login, create_subtask):
        url = reverse('update-subtask', kwargs={'pk': 'wdewew'})
        response = client.post(url)
        assert response.status_code == 500
        assertTemplateUsed(response, 'todo/500_server.html')

    @pytest.mark.django_db
    def test_delete_subtask(self, client, user_login, create_subtask):
        t = Task.objects.get()
        url = reverse('delete-subtask', kwargs={'pk': create_subtask.id})
        response = client.post(url)
        assert response.status_code == 302
        assertRedirects(response, reverse('list-subtask', kwargs={'id': t.id}))

    @pytest.mark.django_db
    def test_delete_subtask_request_without_login_user(self, client):
        response = client.post(reverse('delete-subtask', kwargs={'pk': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_delete_subtask_with_404(self, client, user_login, create_subtask):
        response = client.post(reverse('delete-subtask', kwargs={'pk': 1000}))
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_delete_subtask_with_500(self, client, user_login, create_subtask):
        url = reverse('delete-subtask', kwargs={'pk': 'wdewew'})
        response = client.post(url)
        assert response.status_code == 500
        assertTemplateUsed(response, 'todo/500_server.html')

    @pytest.mark.django_db
    def test_move_subtask(self, client, user_login, create_subtask, create_task2):
        t = Task.objects.get(id=create_task2.id)
        data = {
            "task": t.id,
        }
        url = reverse('move-subtask', kwargs={'pk': create_subtask.id})
        response = client.post(url, data=data)
        assert response.status_code == 302
        assertRedirects(response, reverse('list-subtask', kwargs={'id': t.id}))

    @pytest.mark.django_db
    def test_move_subtask_request_without_login_user(self, client):
        response = client.post(reverse('move-subtask', kwargs={'pk': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_move_subtask_with_404(self, client, user_login, create_subtask):
        response = client.post(reverse('move-subtask', kwargs={'pk': 1000}))
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_move_subtask_with_500(self, client, user_login, create_subtask):
        url = reverse('delete-subtask', kwargs={'pk': 'wdewew'})
        response = client.post(url)
        assert response.status_code == 500
        assertTemplateUsed(response, 'todo/500_server.html')
