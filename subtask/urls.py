from django.urls import path

from subtask import views
from subtask.views import send_mail_to_user

urlpatterns = [
    path('list/subtask/<int:id>/', views.ListSubTaskView.as_view(), name='list-subtask'),
    path('add/subtask/<int:id>/', views.CreateSubTaskView.as_view(), name='add-subtask'),
    path('update/subtask/<pk>/', views.UpdateSubTaskView.as_view(), name='update-subtask'),
    path('delete/subtask/<pk>/', views.DeleteSubTaskView.as_view(), name='delete-subtask'),
    path('move/subtask/<pk>/', views.MoveSubTaskView.as_view(), name='move-subtask'),
    path('view/subtask/<pk>/', views.SubtaskDetailView.as_view(), name='view-subtask'),
    path('send/mail/', send_mail_to_user, name='send-mail'),
]