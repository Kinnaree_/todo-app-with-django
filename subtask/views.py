from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView

import constants
from subtask.form import SubTaskForm, MoveTaskForm, UpdateSubTaskForm
from subtask.models import SubTask
from todo.models import Task
from .tasks import send_mail_func


# Create your views here.
class ListSubTaskView(LoginRequiredMixin, ListView):
    """
           Description : Get all the Sub-Task
    """
    model = SubTask
    template_name = constants.SUBTASK_LIST_PAGE
    context_object_name = 'sub_tasks'

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['task_id'] = self.kwargs['id']
        context_data['tag_id'] = Task.objects.get(id=self.kwargs['id']).tag.id
        context_data['sub_tasks'] = SubTask.objects.filter(task__id=self.kwargs['id'])
        context_data['search_input'] = self.request.GET.get('search-area') or ''
        if context_data['search_input']:
            context_data['sub_tasks'] = context_data['sub_tasks'].filter(title__contains=context_data['search_input'])
        return context_data


class CreateSubTaskView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """
        Description : Add the Sub-Task
    """
    model = SubTask
    form_class = SubTaskForm
    template_name = constants.SUBTASK_ADD_PAGE

    def get_form_kwargs(self):
        kwargs = super(CreateSubTaskView, self).get_form_kwargs()
        initial = kwargs['initial']
        initial['task'] = self.kwargs['id']
        return kwargs

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['task_id'] = self.kwargs['id']
        return context_data

    def get_success_url(self, **kwargs):
        return reverse_lazy('list-subtask', kwargs={'id': self.kwargs['id']})

    success_message = constants.SUB_TASK_ADD


class UpdateSubTaskView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """
        Description : Update the Sub-Task
    """
    model = SubTask
    form_class = UpdateSubTaskForm
    template_name = constants.SUBTASK_UPDATE_PAGE

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['task_id'] = SubTask.objects.get(id=self.kwargs['pk']).task.id
        return context_data

    def get_success_url(self, *args,**kwargs):
        sub_task = SubTask.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('list-subtask', kwargs={'id': sub_task.task.id})

    success_message = constants.SUB_TASK_UPDATE


class DeleteSubTaskView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    """
        Description : Delete the Sub-Task
    """
    model = SubTask
    template_name = constants.SUBTASK_DELETE_PAGE

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['task_id'] = SubTask.objects.get(id=self.kwargs['pk']).task.id
        context_data['sub_task'] = SubTask.objects.get(id=self.kwargs['pk'])
        return context_data

    def get_success_url(self, *args, **kwargs):
        sub_task = SubTask.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('list-subtask', kwargs={'id': sub_task.task.id})

    success_message = constants.SUB_TASK_DELETE


class MoveSubTaskView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = SubTask
    form_class = MoveTaskForm
    template_name = constants.SUBTASK_MOVE_PAGE

    def get_form_kwargs(self):
        kwargs = super(MoveSubTaskView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['task_id'] = SubTask.objects.get(id=self.kwargs['pk']).task.id
        return context_data

    def get_success_url(self, *args, **kwargs):
        sub_task = SubTask.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('list-subtask', kwargs={'id': sub_task.task.id})

    success_message = constants.SUB_TASK_MOVE


class SubtaskDetailView(LoginRequiredMixin, SuccessMessageMixin, DetailView):
    """
            Description : View the SubTask
    """
    model = SubTask
    template_name = constants.SUBTASK_VIEW_PAGE

    def get_context_data(self, **kwargs):
        context_data = super(SubtaskDetailView, self).get_context_data(**kwargs)
        context_data['task_id'] = SubTask.objects.get(id=self.kwargs['pk']).task.id
        context_data['sub_tasks'] = SubTask.objects.filter(id=self.kwargs['pk'])
        return context_data

    def get_success_url(self, *args, **kwargs):
        sub_task = SubTask.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('list-subtask', kwargs={'id': sub_task.task.id})


def send_mail_to_user(request):
    """ view for send reminder mail to user """
    send_mail_func.delay()
    return HttpResponse(constants.MAIL_SUCCESSFULLY_SEND)
