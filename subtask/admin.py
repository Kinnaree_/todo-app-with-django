from django.contrib import admin

from subtask.models import SubTask

# Register your models here.
admin.site.register(SubTask)
