from datetime import datetime

from celery import Celery
from django.core.mail import send_mail
from Todo_app import settings
from .models import Task, SubTask

app = Celery('send_mail_func', broker='redis://localhost:6379')


@app.task
def send_mail_func():
    tasks = Task.objects.filter(complete=False, completion_date=datetime.today().date())
    subtasks = SubTask.objects.filter(complete=False, completion_date=datetime.today().date())
    for task in tasks:
        mail_subject = "ToDos Reminder"
        message = "Today last date for complete your task."
        to_email = task.tag.owner.email
        send_mail(
            subject=mail_subject,
            message=message,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[to_email],
            fail_silently=True,
        )
    for subtask in subtasks:
        mail_subject = "ToDos Reminder"
        message = "Today last date for complete your Sub task."
        to_email = subtask.task.tag.owner.email
        send_mail(
            subject=mail_subject,
            message=message,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[to_email],
            fail_silently=True,
        )
    return "Done"
