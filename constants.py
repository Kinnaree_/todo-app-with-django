DATA_NOT_FOUND = 'Exception: Data not found'

# SUB TASK:
SUB_TASK_ADD = 'Your sub task has been added!'
SUB_TASK_UPDATE = 'Your sub task has been updated!'
SUB_TASK_DELETE = 'Your sub task successfully deleted!'
SUB_TASK_MOVE = 'Your subTask has been moved!'

# MAIL :
MAIL_SUCCESSFULLY_SEND = "Mail successfully send, please check!!"
ACCOUNT_EMAIL_HELP_TEXT = 'Example: useremail@gmail.com'
ACCOUNT_PASSWORD_1_HELP_TEXT = 'Password must have one small letter, one capital letter, one number and one special character. Minimum password length is 7.'

# USER:
EMAIL_IS_TAKEN = 'Email is taken!!'
USERNAME_IS_TAKEN = 'Username is taken!!'
USER_REGISTER = 'User registered successfully!!'
PASSWORD_NOT_MATCH = "Password not match!!"
USER_LOGOUT = 'User successfully logout!!'
USERNAME_TO_SHORT = "Username must be more than 6 character."
PASSWORD_TO_SHORT = "Your password is too short."

# TAG:
TAG_ADD = 'Your Tag Successfully Added!'
TAG_UPDATE = 'Your Tag Successfully Updated!'
TAG_DELETE = 'Your Tag Successfully Deleted!'

# TASK :
TASK_ADD = 'Your task has been added!'
TASK_UPDATE = 'Your task has been updated!'
TASK_DELETE = 'Your task successfully deleted!'

# SUBTASK TEMPLATE PATH VARIABLE:
SUBTASK_LIST_PAGE = 'subtask/list_subtask.html'
SUBTASK_ADD_PAGE = "subtask/add_subtask.html"
SUBTASK_UPDATE_PAGE = 'subtask/update_subtask.html'
SUBTASK_DELETE_PAGE = 'subtask/delete_subtask.html'
SUBTASK_MOVE_PAGE = 'subtask/move_subtask.html'
SUBTASK_VIEW_PAGE = 'subtask/view_subtask.html'

# TASK TEMPLATE PATH VARIABLE:
TASK_LIST_PAGE = 'todo/todo_task_list.html'
TASK_ADD_PAGE = "todo/todo_task_create.html"
TASK_UPDATE_PAGE = 'todo/todo_task_update.html'
TASK_DELETE_PAGE = 'todo/todo_task_delete.html'
TASK_VIEW_PAGE = 'todo/todo_task_view.html'

# TAG TEMPLATE PATH VARIABLE:
TAG_LIST_PAGE = 'todo/todo_tag_list.html'
TAG_ADD_PAGE = "todo/todo_tag_create.html"
TAG_UPDATE_PAGE = 'todo/todo_tag_update.html'
TAG_DELETE_PAGE = 'todo/togo_tag_delete.html'

# USER TEMPLATE PATH VARIABLE:
USER_REGISTER_PAGE = 'users/register.html'

