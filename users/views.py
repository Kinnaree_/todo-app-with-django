from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import View

from constants import USER_REGISTER_PAGE
from users.services import RegisterService, LogoutService


# Create your views here.


class RegisterView(View):
    """
        description: This is user register view.
        GET request will display Register Form in register.html page.
        POST request will make user registered if details is valid else register
        form with error is displayed.
    """

    template_name = USER_REGISTER_PAGE

    def get(self, request):
        context = RegisterService.get_context_data()
        return render(request, RegisterView.template_name, context=context)

    def post(self, request):
        return RegisterService.is_forms_valid(request=request, template_name=RegisterView.template_name)


class LogoutView(LoginRequiredMixin, View):
    """
    description: This is user logout view.
    GET request will log out user and redirects to home page.
    """

    def get(self, request):
        return LogoutService.logout_user(request=request)

