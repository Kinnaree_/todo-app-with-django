from django.contrib.auth.views import LoginView, PasswordChangeView, PasswordResetView, PasswordResetDoneView, \
     PasswordResetCompleteView
from django.urls import reverse, resolve

from users.views import RegisterView, LogoutView


class TestUsersUrls(object):
    """Test for users app's urls.
    Below test functions tests for all urls defined in users/urls.py
    """

    def test_register_url(self, client):
        url = reverse('register')
        assert resolve(url).func.view_class == RegisterView

    def test_login_url(self):
        url = reverse('login')
        assert resolve(url).func.view_class == LoginView

    def test_logout_url(self):
        url = reverse('logout')
        assert resolve(url).func.view_class == LogoutView

    def test_change_password_url(self):
        url = reverse('change_password')
        assert resolve(url).func.view_class == PasswordChangeView

    def test_reset_password_url(self):
        url = reverse('password_reset')
        assert resolve(url).func.view_class == PasswordResetView

    def test_reset_password_done_url(self):
        url = reverse('password_reset_done')
        assert resolve(url).func.view_class == PasswordResetDoneView

    def test_reset_password_complete_url(self):
        url = reverse('password_reset_complete')
        assert resolve(url).func.view_class == PasswordResetCompleteView