import pytest
from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed, assertRedirects


class TestRegisterView:

    url = reverse('register')

    @pytest.mark.django_db
    def test_get_request(self, client):
        response = client.get(self.url)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_success(self, client):
        data = {
            "username": "welcome",
            "password1": "Ladani123@",
            "password2": "Ladani123@",
            "email": "kinnaree.inexture@gmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 302
        assertRedirects(response, '/')

    @pytest.mark.django_db
    def test_user_register_invalid_password_format(self, client):
        data = {
            "username": "welcome",
            "password1": "ladani123@",
            "password2": "ladani123@",
            "email": "kinnaree.inexture@gmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_invalid_password_least_one_lowercase(self, client):
        data = {
            "username": "welcome",
            "password1": "LADANI123@",
            "password2": "LADANI123@",
            "email": "kinnaree.inexture@gmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_invalid_password_least_one_digit(self, client):
        data = {
            "username": "welcome",
            "password1": "Ladaniiii@",
            "password2": "Ladaniiii@",
            "email": "kinnaree.inexture@gmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_invalid_password_least_one_special_character(self, client):
        data = {
            "username": "welcome",
            "password1": "Ladani1234",
            "password2": "Ladani1234",
            "email": "kinnaree.inexture@gmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_password_not_matched(self, client):
        data = {
            "username": "welcome",
            "password1": "Ladani123@",
            "password2": "Ladani123",
            "email": "kinnaree@gmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_with_same_username(self, client, create_user):
        data = {
            "username": "kinnaree",
            "password1": "Ladani123@",
            "password2": "Ladani123@",
            "email": "kinnaree@gmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_submit_blank_form(self, client):
        response = client.post(self.url)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_missing_parameter(self, client):
        data = {
            "password1": "Ladani123@",
            "password2": "Ladani123@",
            "email": "kinnaree@gmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_invalid_email_format(self, client):
        data = {
            "username": "kinnaree",
            "password1": "Ladani123@",
            "password2": "Ladani123@",
            "email": "kinnareegmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')

    @pytest.mark.django_db
    def test_user_register_with_same_email_id(self, client, create_user):
        data = {
            "username": "kinnaree",
            "password1": "Ladani123@",
            "password2": "Ladani123@",
            "email": "kinnaree.inexture@gmail.com"
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/register.html')


class TestLoginView:
    url = reverse('login')

    @pytest.mark.django_db
    def test_user_login_success(self, client, test_username, test_password, create_user):
        data = {
            'username': test_username,
            'password': test_password,
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 302
        assertRedirects(response, reverse('tag_list'))

    @pytest.mark.django_db
    def test_user_login_invalid_password(self, client, test_username, test_password, create_user):
        data = {
            'username': test_username,
            'password': "password",
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/login.html')

    @pytest.mark.django_db
    def test_user_login_invalid_username(self, client, test_password, create_user):
        data = {
            'username': 'hello',
            'password': test_password
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/login.html')

    def test_get_login_form(self, client):
        response = client.get(self.url)
        assert response.status_code == 200

    @pytest.mark.django_db
    def test_user_login_missing_parameter(self, client):
        data = {
            'username': 'hello',
        }
        response = client.post(self.url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'users/login.html')


class TestLogoutView:
    @pytest.mark.django_db
    def test_logout_for_logged_in_user(self, login_user, create_user, test_password, test_username):

        client, user = login_user(username=test_username, password=test_password)
        url = reverse('logout')
        response = client.get(url)
        assert response.status_code == 302
        assertRedirects(response, reverse('login'))


