from django.contrib import messages
from django.contrib.auth import logout
from django.shortcuts import render, redirect

import constants
from constants import USER_LOGOUT
from users.form import RegisterForm


class RegisterService:
    @staticmethod
    def get_context_data():
        return {
            'form': RegisterForm(),
        }

    @staticmethod
    def is_forms_valid(request, template_name):
        form = RegisterForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, constants.USER_REGISTER)
            return redirect('login')
        return render(request, template_name, context={'form': form})


class LogoutService:

    @staticmethod
    def logout_user(request):
        logout(request)
        messages.success(request, USER_LOGOUT)
        return redirect('login')


