from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from Todo_app.validation import validate_password
from constants import ACCOUNT_EMAIL_HELP_TEXT, ACCOUNT_PASSWORD_1_HELP_TEXT, EMAIL_IS_TAKEN, PASSWORD_NOT_MATCH


class RegisterForm(forms.ModelForm):
    email = forms.EmailField(
        required=True,
        help_text=ACCOUNT_EMAIL_HELP_TEXT
    )
    password = forms.CharField(
        widget=forms.PasswordInput(),
        required=True,
        validators=[validate_password],
        help_text=ACCOUNT_PASSWORD_1_HELP_TEXT
    )
    confirm_password = forms.CharField(
        widget=forms.PasswordInput(),
        required=True
    )

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'confirm_password']

    def clean_email(self):
        email = self.data.get('email')
        if User.objects.filter(email=email).exists():
            self.add_error('email', EMAIL_IS_TAKEN)
        return email

    def clean_password2(self):
        confirm_password = self.data.get('confirm_password')
        password = self.data.get('password')
        if password != confirm_password:
            self.add_error('confirm_password', PASSWORD_NOT_MATCH)
        return confirm_password

    def save(self, **kwargs):
        email = self.data.get('email')
        password = self.data.get('password')
        data = {
            'username': self.data.get('username'),
        }

        user = User.objects.create_user(email=email, password=password, **data)
        # user.is_active = False
        user.save()

        return user


