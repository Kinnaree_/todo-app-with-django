import pytest
from django.contrib.auth.models import User
from django.test.client import Client

from subtask.models import SubTask
from todo.models import Tag, Task


@pytest.fixture
def get_client(client):
    def inner_function():
        return Client()

    return inner_function


@pytest.fixture()
def create_user():
    return User.objects.create_user(username='kinnaree', password='Ladani123@', email='kinnaree.inexture@gmail.com')


@pytest.fixture
def user_login(client, create_user, test_username, test_password):
    response = client.post('/', dict(username=test_username, password=test_password))
    return response


@pytest.fixture()
def create_tag(create_user):
    owner = User.objects.get(username='kinnaree')
    return Tag.objects.create(tag='django', owner=owner)


@pytest.fixture()
def create_task(create_tag):
    t = Tag.objects.get()
    return Task.objects.create(title='DRF', detail='learn', tag=t, completion_date="2023-02-26")


@pytest.fixture()
def create_task2(create_tag):
    t = Tag.objects.get()
    return Task.objects.create(title='Flask', detail='practice project', tag=t, completion_date="2023-02-28")


@pytest.fixture()
def create_subtask(create_task):
    t = Task.objects.get()
    return SubTask.objects.create(title='demo', detail='learning', task=t, completion_date="2023-02-26")


@pytest.fixture
def test_username(client):
    return "kinnaree"


@pytest.fixture
def test_password(client):
    return "Ladani123@"


@pytest.fixture
def login_user(
        db,
        get_client
):
    def inner_function(username, password):
        data = dict(username=username, password=password)
        client = get_client()
        client.login(**data)
        return client, User.objects.get(username=username)
    return inner_function


