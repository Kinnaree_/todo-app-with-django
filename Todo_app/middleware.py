from django.shortcuts import render


class ExceptionHandleMiddleware:
    """
    Custom Middleware to render Page
    """

    def __init__(self, get_response):
        """
        One-time configuration and initialisation.
        """
        self.get_response = get_response

    def __call__(self, request):
        """
        Code to be executed for each request before the view (and later
        middleware) are called.
        """

        response = self.get_response(request)
        if response.status_code == 404:
            return render(
                request=request,
                template_name='todo/404_page_not_found.html',
                status=404
            )
        return response

    def process_exception(self, request, exception):
        # This is the method that responsible for the safe-exception handling
        if isinstance(exception, ValueError):
            return render(
                request=request,
                template_name='todo/500_server.html',
                status=500
            )
        return None