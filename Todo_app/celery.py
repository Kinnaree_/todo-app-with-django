from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Todo_app.settings')
app = Celery('Todo_app')
app.conf.enable_utc = False

app.conf.update(timezone='Asia/Kolkata')
app.config_from_object(settings, namespace='CELERY')

# # CELERY BEAT SETTING
app.conf.beat_schedule = {
    # schedulers
    'send-reminder-mail': {
        'task': "subtask.tasks.send_mail_func",
        'schedule': crontab(minute=0, hour=0),
        # 'args':(2,)
    }
}
app.autodiscover_tasks()

