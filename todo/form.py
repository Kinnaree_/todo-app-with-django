from django import forms

from todo.models import Tag, Task
from todo.validation import validate_date


class CreateUpdateTagTodoForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = "__all__"
        widgets = {'owner': forms.HiddenInput()}


class CreateTaskForm(forms.ModelForm):
    completion_date = forms.DateField(
        required=False,
        validators=[validate_date]
    )

    class Meta:
        model = Task
        fields = ('title', 'detail', 'completion_date', 'tag')
        widgets = {'tag': forms.HiddenInput()}


class UpdateTaskForm(forms.ModelForm):
    completion_date = forms.DateField(
        required=False,
        validators=[validate_date]
    )

    class Meta:
        model = Task
        fields = '__all__'
        widgets = {'tag': forms.HiddenInput()}
