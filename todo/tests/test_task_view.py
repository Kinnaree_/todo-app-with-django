import pytest
from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed, assertRedirects

from todo.models import Tag, Task


class TestTaskView:

    @pytest.mark.django_db
    def test_get_request(self, client, user_login):
        url = reverse('list', kwargs={'id': 1})
        response = client.get(url)
        assert response.status_code == 200
        assertTemplateUsed(response, 'todo/todo_task_list.html')

    @pytest.mark.django_db
    def test_get_task_request_without_login_user(self, client):
        response = client.post(reverse('list', kwargs={'id': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_get_task_with_404(self, client, user_login, create_tag):
        response = client.post('tag/list/1000/')
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_create_task(self, client, user_login, create_tag):
        t = Tag.objects.get()
        data = {
            "title": "pagination",
            "detail": "learn",
            "tag": t.id,
            "completion_date": "2023-02-24"
        }
        url = reverse('create_task', kwargs={'id': t.id})
        response = client.post(url, data=data)
        assert response.status_code == 302
        assertRedirects(response, reverse('list', kwargs={'id': t.id}))

    @pytest.mark.django_db
    def test_create_task_with_invalid_date(self, client, user_login, create_tag):
        t = Tag.objects.get()
        data = {
            "title": "pagination",
            "detail": "learn",
            "tag": t.id,
            "completion_date": "2023-02-15"
        }
        url = reverse('create_task', kwargs={'id': t.id})
        response = client.post(url, data=data)
        assert response.status_code == 200
        assertTemplateUsed(response, 'todo/todo_task_create.html')

    @pytest.mark.django_db
    def test_create_task_request_without_login_user(self, client):
        response = client.post(reverse('create_task', kwargs={'id': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_create_task_with_404(self, client, user_login, create_tag):
        response = client.post('task/create/1000/')
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_update_task(self, client, user_login, create_task):
        t = Tag.objects.get()
        data = {
            "title": "salary",
            "detail": "learn",
            "tag": t.id,
            "complete": "True",
            "completion_date": "2023-02-24"
        }
        url = reverse('update_task', kwargs={'pk': create_task.id})
        response = client.post(url, data=data)
        assert response.status_code == 302
        assertRedirects(response, reverse('list', kwargs={'id': t.id}))

    @pytest.mark.django_db
    def test_update_task_request_without_login_user(self, client):
        response = client.post(reverse('update_task', kwargs={'pk': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_update_task_with_404(self, client, user_login, create_task):
        response = client.post(reverse('update_task', kwargs={'pk': 1000}))
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_update_task_with_500(self, client, user_login, create_task):
        url = reverse('update_task', kwargs={'pk': 'wdewew'})
        response = client.post(url)
        assert response.status_code == 500
        assertTemplateUsed(response, 'todo/500_server.html')

    @pytest.mark.django_db
    def test_delete_task(self, client, user_login, create_task):
        t = Tag.objects.get()
        url = reverse('delete_task', kwargs={'pk': create_task.id})
        response = client.post(url)
        assert response.status_code == 302
        assertRedirects(response, reverse('list', kwargs={'id': t.id}))

    @pytest.mark.django_db
    def test_delete_task_request_without_login_user(self, client):
        response = client.post(reverse('delete_task', kwargs={'pk': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_delete_task_with_404(self, client, user_login, create_task):
        response = client.post(reverse('delete_task', kwargs={'pk': 1000}))
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_delete_task_with_500(self, client, user_login, create_task):
        url = reverse('delete_task', kwargs={'pk': 'wdewew'})
        response = client.post(url)
        assert response.status_code == 500
        assertTemplateUsed(response, 'todo/500_server.html')

