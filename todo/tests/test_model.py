import pytest

from todo.models import Tag, Task


class TestTag:

    @pytest.mark.django_db
    def test_str_method(self, create_tag):
        obj = Tag.objects.get(tag='django')
        assert str(obj) == 'django'

    @pytest.mark.django_db
    def test_fields(self, create_tag):
        obj = Tag.objects.get(tag='django')
        assert obj.tag == 'django'


class TestTask:

    @pytest.mark.django_db
    def test_str_method(self, create_task):
        obj = Task.objects.get(title='DRF')
        assert str(obj) == 'DRF'

    @pytest.mark.django_db
    def test_fields(self, create_task):
        obj = Task.objects.get()
        assert obj.title == 'DRF'
        assert obj.detail == 'learn'
        assert str(obj.tag) == 'django'
        assert str(obj.completion_date) == '2023-02-26'
