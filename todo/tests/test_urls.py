from django.urls import reverse, resolve

from todo.views import Listview, CreateTodoTag, UpdateTodoTag, DeleteTodoTag, ListTaskView, CreateTaskView, \
    UpdateTaskView, DeleteTaskView


class TestTagsUrls:
    """Test for tag urls.
    Below test functions tests for all urls defined in tag/urls.py
    """

    def test_tag_get_url(self):
        url = reverse('tag_list')
        assert resolve(url).func.view_class == Listview

    def test_tag_add_url(self):
        url = reverse('create_tag')
        assert resolve(url).func.view_class == CreateTodoTag

    def test_tag_update_url(self):
        url = reverse('update_tag', kwargs={'pk': 1})
        assert resolve(url).func.view_class == UpdateTodoTag

    def test_tag_delete_url(self):
        url = reverse('delete_tag', kwargs={'pk': 1})
        assert resolve(url).func.view_class == DeleteTodoTag


class TestTaskUrls:
    """Test for task urls.
    Below test functions tests for all urls defined in task/urls.py
    """

    def test_task_get_url(self):
        url = reverse('list', kwargs={'id': 1})
        assert resolve(url).func.view_class == ListTaskView

    def test_task_add_url(self):
        url = reverse('create_task', kwargs={'id': 1})
        assert resolve(url).func.view_class == CreateTaskView

    def test_task_update_url(self):
        url = reverse('update_task', kwargs={'pk': 1})
        assert resolve(url).func.view_class == UpdateTaskView

    def test_task_delete_url(self):
        url = reverse('delete_task', kwargs={'pk': 1})
        assert resolve(url).func.view_class == DeleteTaskView
