import pytest
from django.contrib.auth.models import User
from django.urls import reverse
from pytest_django.asserts import assertRedirects, assertTemplateUsed

from todo.models import Tag


class TestTagView:

    @pytest.mark.django_db
    def test_create_tag(self, client, user_login, test_username):
        data = {
            "tag": "welcome",
            "owner": User.objects.get(username=test_username).id
        }
        url = reverse('create_tag')
        response = client.post(url, data)
        assert response.status_code == 302
        assertRedirects(response, reverse('tag_list'))

    @pytest.mark.django_db
    def test_create_tag_request_without_login_user(self, client):
        response = client.post(reverse('tag_list'))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_update_tag(self, client, user_login, create_tag, test_username):
        owner = User.objects.get(username=test_username)
        t = Tag.objects.get()
        data = {
            "tag": "DRF",
            "owner": owner.id
        }
        url = reverse('update_tag', kwargs={'pk': t.id})
        response = client.post(url, data=data)
        t.refresh_from_db()
        assert response.status_code == 302
        assertRedirects(response, reverse('tag_list'))

    @pytest.mark.django_db
    def test_update_tag_request_without_login_user(self, client):
        response = client.post(reverse('update_tag', kwargs={'pk': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_update_tag_with_404(self, client, user_login, create_tag, test_username):
        response = client.post(reverse('update_tag', kwargs={'pk': 1000}))
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_update_tag_with_500(self, client, user_login, create_tag, test_username):
        url = reverse('update_tag', kwargs={'pk': 'wdewew'})
        response = client.post(url)
        assert response.status_code == 500
        assertTemplateUsed(response, 'todo/500_server.html')

    @pytest.mark.django_db
    def test_delete_tag(self, client, user_login, create_tag):
        t = Tag.objects.get()
        url = reverse('delete_tag', kwargs={'pk': t.id})
        response = client.post(url)
        assert response.status_code == 302
        assertRedirects(response, reverse('tag_list'))

    @pytest.mark.django_db
    def test_delete_tag_request_without_login_user(self, client):
        response = client.post(reverse('delete_tag', kwargs={'pk': 1}))
        assert response.status_code == 302

    @pytest.mark.django_db
    def test_delete_tag_with_404(self, client, user_login, create_tag):
        response = client.post(reverse('delete_tag', kwargs={'pk': 1000}))
        assert response.status_code == 404
        assertTemplateUsed(response, 'todo/404_page_not_found.html')

    @pytest.mark.django_db
    def test_delete_tag_with_500(self, client, user_login, create_tag):
        url = reverse('delete_tag', kwargs={'pk': 'wdewew'})
        response = client.post(url)
        assert response.status_code == 500
        assertTemplateUsed(response, 'todo/500_server.html')

    @pytest.mark.django_db
    def test_get_request(self, client, user_login):
        url = reverse('tag_list')
        response = client.get(url)
        assert response.status_code == 200
        assertTemplateUsed(response, 'todo/todo_tag_list.html')

    @pytest.mark.django_db
    def test_get_request_without_login_user(self, client):
        response = client.post(reverse('tag_list'))
        assert response.status_code == 302
