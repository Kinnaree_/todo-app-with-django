from django.core.exceptions import ValidationError
from django.utils import timezone


def validate_date(completion_date):
    if completion_date < timezone.now().date():
        raise ValidationError(message='Date cannot be in the past')
