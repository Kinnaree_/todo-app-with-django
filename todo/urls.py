from django.urls import path

from todo import views

urlpatterns = [
    path('tag/list/', views.Listview.as_view(), name='tag_list'),
    path('tag/create/', views.CreateTodoTag.as_view(), name='create_tag'),
    path('tag/update/<pk>/', views.UpdateTodoTag.as_view(), name='update_tag'),
    path('tag/delete/<pk>', views.DeleteTodoTag.as_view(), name='delete_tag'),

    path('task/list/<int:id>', views.ListTaskView.as_view(), name='list'),
    path('task/view/<pk>', views.ListDetailView.as_view(), name='detail'),
    path('task/create/<int:id>', views.CreateTaskView.as_view(), name='create_task'),
    path('task/update/<pk>', views.UpdateTaskView.as_view(), name='update_task'),
    path('task/delete/<pk>', views.DeleteTaskView.as_view(), name='delete_task'),
]
