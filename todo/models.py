from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Tag(models.Model):
    tag = models.CharField(max_length=100)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='owner_name')

    def __str__(self):
        return self.tag


class Task(models.Model):
    title = models.CharField(max_length=100)
    detail = models.TextField(max_length=200, blank=True)
    complete = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    completion_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['complete']

