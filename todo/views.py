from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils import timezone
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView

import constants
from todo.form import CreateTaskForm, CreateUpdateTagTodoForm, UpdateTaskForm
from todo.models import Task, Tag

# Create your views here.

# Tag module CURD :


class Listview(LoginRequiredMixin, ListView):
    """
       Description : Get all the Tag
    """
    model = Tag
    template_name = constants.TAG_LIST_PAGE
    context_object_name = 'tags'

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['tags'] = Tag.objects.filter(owner=self.request.user)
        context_data['search_input'] = self.request.GET.get('search-area') or ''
        if context_data['search_input']:
            context_data['tags'] = context_data['tags'].filter(tag__contains=context_data['search_input'])
        return context_data


class CreateTodoTag(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """
        Description : Add the Tag
    """
    model = Tag
    form_class = CreateUpdateTagTodoForm
    template_name = constants.TAG_ADD_PAGE
    success_url = reverse_lazy('tag_list')

    def get_form_kwargs(self):
        kwargs = super(CreateTodoTag, self).get_form_kwargs()
        initial = kwargs['initial']
        initial['owner'] = self.request.user
        return kwargs

    success_message = constants.TAG_ADD


class UpdateTodoTag(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """
        Description : Update the Tag
    """
    model = Tag
    form_class = CreateUpdateTagTodoForm
    template_name = constants.TAG_UPDATE_PAGE
    success_url = reverse_lazy('tag_list')

    success_message = constants.TAG_UPDATE


class DeleteTodoTag(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    """
        Description : Delete the Tag
    """
    model = Tag
    template_name = constants.TAG_DELETE_PAGE
    success_url = reverse_lazy('tag_list')

    success_message = constants.TAG_DELETE


# Task module CURD:
class ListTaskView(LoginRequiredMixin, ListView):
    """
        Description : Get all the Task
    """
    model = Task
    template_name = constants.TASK_LIST_PAGE
    context_object_name = 'tasks'

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['tags'] = self.kwargs['id']
        context_data['tasks'] = Task.objects.filter(tag_id=self.kwargs['id'])
        context_data['count'] = Task.objects.filter(complete=False, tag=self.kwargs['id']).count()
        context_data['search_input'] = self.request.GET.get('search-area') or ''
        if context_data['search_input']:
            context_data['tasks'] = context_data['tasks'].filter(title__contains=context_data['search_input'])
        return context_data


class CreateTaskView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """
        Description : Add the Task
    """
    model = Task
    form_class = CreateTaskForm
    template_name = constants.TASK_ADD_PAGE

    def get_form_kwargs(self):
        kwargs = super(CreateTaskView, self).get_form_kwargs()
        initial = kwargs['initial']
        initial['tag'] = self.kwargs['id']
        return kwargs

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['tag_id'] = self.kwargs['id']
        return context_data

    def get_success_url(self, **kwargs):
        return reverse_lazy('list', kwargs={'id': self.kwargs['id']})

    success_message = constants.TASK_ADD


class UpdateTaskView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """
        Description : Update the Task
    """
    model = Task
    form_class = UpdateTaskForm
    template_name = constants.TASK_UPDATE_PAGE

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['tag_id'] = Task.objects.get(id=self.kwargs['pk']).tag.id
        return context_data

    def get_success_url(self, **kwargs):
        task = Task.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('list', kwargs={'id': task.tag.id})

    success_message = constants.TASK_UPDATE


class DeleteTaskView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    """
        Description : Delete the Task
    """
    model = Task
    template_name = constants.TASK_DELETE_PAGE

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['tag_id'] = Task.objects.get(id=self.kwargs['pk']).tag.id
        return context_data

    def get_success_url(self, **kwargs):
        task = Task.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('list', kwargs={'id': task.tag.id})

    success_message = constants.TASK_DELETE


class ListDetailView(LoginRequiredMixin, SuccessMessageMixin, DetailView):
    """
            Description : View the Task
    """
    model = Task
    template_name = constants.TASK_VIEW_PAGE

    def get_context_data(self, **kwargs):
        context_data = super(ListDetailView, self).get_context_data(**kwargs)
        context_data['tag_id'] = Task.objects.get(id=self.kwargs['pk']).tag.id
        context_data['tasks'] = Task.objects.filter(id=self.kwargs['pk'])
        return context_data

    def get_success_url(self, **kwargs):
        task = Task.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('list', kwargs={'id': task.tag.id})



